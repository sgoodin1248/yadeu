﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Security.Principal;

namespace YADEU
{
   
    public enum ErasureAlgorithm
    {
        Pseudorandom_1,
        Zeroes_1,
        Ones_1,
        DoD_5220_22_M_3
    }

    static class Config {
        public static ErasureAlgorithm ErasureAlgorithm;
        public static List<Container> Containers;
    }

    static class Global
    {
        public static string currentErasingDrive = "";
        public static string containerPath = "";
        public static string containerAlias = "";
        public static string password = "";
        public static bool copying = false;
        public static bool copyLock = false;
    }

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            if (System.Diagnostics.Process.GetProcessesByName(System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location)).Count() > 1)
            {
                MessageBox.Show("An instance of this application is already running.", "Error On Startup", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return; // Exit if existing is running
            }

            if ( !(new WindowsPrincipal(WindowsIdentity.GetCurrent()))
                .IsInRole(WindowsBuiltInRole.Administrator) )
            {
                MessageBox.Show("You must run this program as an administrator.", "Error On Startup", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Config.Containers = new List<Container>();

            Config.ErasureAlgorithm = ErasureAlgorithm.Pseudorandom_1;
            if (!File.Exists("config"))
            {
                using (StreamWriter sw = new StreamWriter(File.Create("config")))
                {
                    sw.WriteLine("WipeDefault = " + ErasureAlgorithm.Pseudorandom_1.ToString());
                    sw.WriteLine("\r\nContainers:\r\n");
                }
            }
            else
            {
                using (StreamReader sr = new StreamReader("config"))
                {
                    Regex erasureregex = new Regex("WipeDefault *= *(.*)");
                    Regex containerregex = new Regex(@"(.*?)\|(.*?)\|(.*)");

                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();
                        if (erasureregex.IsMatch(line))
                            Config.ErasureAlgorithm = (ErasureAlgorithm) Enum.Parse(typeof(ErasureAlgorithm), erasureregex.Match(line).Groups[1].ToString());
                        else if ( containerregex.IsMatch(line) )
                        {
                            Match match = containerregex.Match(line);
                            Container container = new Container(match.Groups[1].ToString(), match.Groups[2].ToString(), match.Groups[3].ToString().ToUpper() + @":\");
                            Config.Containers.Add(container);
                        }
                    }
                }
            }
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Main());            
        }        
    }
}
