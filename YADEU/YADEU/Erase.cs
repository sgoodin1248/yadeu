﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Security.Cryptography;
using System.Windows.Forms;

namespace YADEU
{
    static class Erase
    {
        private static bool diskEraseRunning = false;
        private static bool eraseRunning = false;

        public static void EraseFile(string path, ErasureAlgorithm algorithm, long size = 4096)
        {
            eraseRunning = true;
            switch ( algorithm )
            {
                case ErasureAlgorithm.DoD_5220_22_M_3:
                    Dod_5220_22_M(path, size);
                    break;
                case ErasureAlgorithm.Ones_1:
                    Ones_1(path, size);
                    break;
                case ErasureAlgorithm.Pseudorandom_1:
                    Pseudorandom_1(path, size);
                    break;
                case ErasureAlgorithm.Zeroes_1:
                    Zeroes_1(path, size);
                    break;
                default:
                    Pseudorandom_1(path, size);
                    break;
            }
            eraseRunning = false;
            File.Delete(path);
        }

        private static void Dod_5220_22_M(string path, long size = 4096)
        {
            Zeroes_1(path, size);
            Ones_1(path, size);
            Pseudorandom_1(path, size);
        }

        private static void Ones_1(string path, long size = 4096)
        {
            byte[] ones = new Byte[Math.Min(4096, size)];
            long length;

            for (int i = 0; i < ones.Length; i++)
                ones[i] = 1;

            using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.ReadWrite))
            {
                if(fs.Length % 4096 == 0)
                    length = fs.Length / 4096;
                else
                    length = (fs.Length + 4096 - (fs.Length % 4096)) / 4096;
                using (BinaryWriter bw = new BinaryWriter(fs))
                {
                    if (!eraseRunning && !diskEraseRunning)
                        return;
                    for (long i = 0; i < length; i++)
                        bw.Write(ones);
                }
            }
        }

        private static void Zeroes_1(string path, long size = 4096)
        {
            byte[] zeroes = new Byte[Math.Min(4096, size)];
            long length;

            for (int i = 0; i < zeroes.Length; i++)
                zeroes[i] = 0;

            using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.ReadWrite))
            {
                if (fs.Length % 4096 == 0)
                    length = fs.Length / 4096;
                else
                    length = (fs.Length + 4096 - (fs.Length % 4096)) / 4096;
                using (BinaryWriter bw = new BinaryWriter(fs))
                {
                    for (long i = 0; i < length; i++)
                    {
                        if (!eraseRunning && !diskEraseRunning)
                            return;
                        bw.Write(zeroes);
                    }
                }
            }
        }

        private static void Pseudorandom_1(string path, long size = 4096)
        {
            byte[] random = new Byte[Math.Min(4096, size)];
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            long length;
                        
            using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.ReadWrite))
            {
                if (fs.Length % 4096 == 0)
                    length = fs.Length / 4096;
                else
                    length = (fs.Length + 4096 - (fs.Length % 4096)) / 4096;
                using (BinaryWriter bw = new BinaryWriter(fs))
                {
                    for (long i = 0; i < length; i++)
                    {
                        if (!eraseRunning && !diskEraseRunning)
                            return;
                        rng.GetNonZeroBytes(random);
                        bw.Write(random);
                    }
                }
            }
        }

        public static void diskErase(string drive)
        {
            diskEraseRunning = true;
            if (Directory.Exists(drive + "YADEUEraseDir"))
                Erase.EraseDirectory(drive + "YADEUEraseDir", false);
            EraseProgress ep = new EraseProgress();
            try
            {
                DriveInfo di = DriveInfo.GetDrives().FirstOrDefault(d => d.Name == drive);
                if (!Directory.Exists(drive + "YADEUEraseDir"))
                {
                    Directory.CreateDirectory(drive + "YADEUEraseDir");
                    DirectoryInfo dinfo = new DirectoryInfo(drive + "YADEUEraseDir");
                    dinfo.Attributes = FileAttributes.Hidden;
                }

                long i = 0;
                long multiplier = 16384;
                long length = 4096;
                long previouslength = long.MaxValue;
                double originalLength = (double) di.AvailableFreeSpace;

                ep.Show();
                ep.Progress = 0;

                while (diskEraseRunning)
                {
                    Application.DoEvents();
                    string path = drive + @"YADEUEraseDir\_" + i.ToString();
                    i++;
                    if (length == 0)
                        break;
                    using (FileStream fs = new FileStream(path, FileMode.CreateNew, FileAccess.ReadWrite))
                    {
                        fs.SetLength(length);
                    }

                    switch (Config.ErasureAlgorithm)
                    {
                        case ErasureAlgorithm.DoD_5220_22_M_3:
                            Dod_5220_22_M(path, length);
                            break;
                        case ErasureAlgorithm.Ones_1:
                            Ones_1(path, length);
                            break;
                        case ErasureAlgorithm.Pseudorandom_1:
                            Pseudorandom_1(path, length);
                            break;
                        case ErasureAlgorithm.Zeroes_1:
                            Zeroes_1(path, length);
                            break;
                        default:
                            Pseudorandom_1(path, length);
                            break;
                    }
                    di = DriveInfo.GetDrives().FirstOrDefault(d => d.Name == drive);                    
                    if (previouslength == di.AvailableFreeSpace || length < 4096)
                    {                        
                        if (di.AvailableFreeSpace == 0)
                        {
                            ep.Progress = 100;
                            break;
                        }
                        length /= 2;
                        previouslength = di.AvailableFreeSpace;
                        continue;
                    }
                    while (di.AvailableFreeSpace <= 4096 * multiplier && multiplier >= 2)
                        multiplier /= 2;
                    length = 4096 * multiplier;
                    if (di.AvailableFreeSpace < 4096)
                        length = di.AvailableFreeSpace;
                    previouslength = di.AvailableFreeSpace;
                    ep.Progress = Convert.ToInt32(100.0 * ((originalLength - ((double)previouslength)) / originalLength));
                }

                ep.Progress = 100;
                ep.Close();

                DirectoryInfo dinfo2 = new DirectoryInfo(drive + @"YADEUEraseDir");

                foreach (FileInfo fi in dinfo2.GetFiles())
                {
                    fi.Delete();
                }

                Directory.Delete(drive + @"YADEUEraseDir");
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error ocurred in the disk erase process:\r\n\r\n" + ex.Message, "Disk Erase Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ep.Close();
            }
            finally
            {
                diskEraseRunning = false;
            }
        }

        public static void EraseDirectory(string path, bool secureErase, string[] ignore = null)
        {
            DirectoryInfo di = new DirectoryInfo(path);
            if (ignore == null)
                ignore = new string[] { };
            foreach(DirectoryInfo dir in di.GetDirectories())
            {
                if ( !ignore.Contains(di.Name) )
                    EraseDirectory(path + @"\" + dir.Name, secureErase, null);
            }

            foreach(FileInfo file in di.GetFiles())
            {
                if (secureErase)
                    EraseFile(path + @"\" + file.Name, Config.ErasureAlgorithm);
                else
                    File.Delete(path + @"\" + file.Name);
            }

            try {
                Directory.Delete(path, true);
            } catch(Exception)
            {
                Directory.Delete(path, true);
            }

        }
        
        public static void stopDiskErase()
        {
            diskEraseRunning = false;
        }     

        public static void stopFileErase()
        {
            eraseRunning = false;
        }
    }
}
