﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YADEU
{
    public partial class EraseProgress : Form
    {
        public int Progress
        {
            set
            {
                progressBar1.Value = value;
            }
        }
        public EraseProgress()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var response = MessageBox.Show("Are you sure you want to cancel the process?", "Cancel Erase?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
            if ( response == DialogResult.Yes )
            {
                Erase.stopDiskErase();
                this.Close();
            }
        }
    }
}
