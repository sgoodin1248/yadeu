﻿namespace YADEU
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createEncryptedContainerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openEncryptedContainerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eraseFreeDiskSpaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.containerlistBox = new System.Windows.Forms.ListBox();
            this.openButton = new System.Windows.Forms.Button();
            this.fileQueuelistBox = new System.Windows.Forms.ListBox();
            this.containerFilesFolderlistBox = new System.Windows.Forms.ListBox();
            this.addfileButton = new System.Windows.Forms.Button();
            this.removeButton = new System.Windows.Forms.Button();
            this.toContainerButton = new System.Windows.Forms.Button();
            this.fromContainerButton = new System.Windows.Forms.Button();
            this.addFolderButton = new System.Windows.Forms.Button();
            this.removeContainerButton = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(678, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createEncryptedContainerToolStripMenuItem,
            this.openEncryptedContainerToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // createEncryptedContainerToolStripMenuItem
            // 
            this.createEncryptedContainerToolStripMenuItem.Name = "createEncryptedContainerToolStripMenuItem";
            this.createEncryptedContainerToolStripMenuItem.Size = new System.Drawing.Size(228, 22);
            this.createEncryptedContainerToolStripMenuItem.Text = "Create Encrypted Container...";
            this.createEncryptedContainerToolStripMenuItem.Click += new System.EventHandler(this.createEncryptedContainerToolStripMenuItem_Click);
            // 
            // openEncryptedContainerToolStripMenuItem
            // 
            this.openEncryptedContainerToolStripMenuItem.Name = "openEncryptedContainerToolStripMenuItem";
            this.openEncryptedContainerToolStripMenuItem.Size = new System.Drawing.Size(228, 22);
            this.openEncryptedContainerToolStripMenuItem.Text = "Open Encrypted Container...";
            this.openEncryptedContainerToolStripMenuItem.Click += new System.EventHandler(this.openEncryptedContainerToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(228, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.eraseFreeDiskSpaceToolStripMenuItem,
            this.preferencesToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // eraseFreeDiskSpaceToolStripMenuItem
            // 
            this.eraseFreeDiskSpaceToolStripMenuItem.Name = "eraseFreeDiskSpaceToolStripMenuItem";
            this.eraseFreeDiskSpaceToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.eraseFreeDiskSpaceToolStripMenuItem.Text = "Erase Free Disk Space";
            this.eraseFreeDiskSpaceToolStripMenuItem.Click += new System.EventHandler(this.eraseFreeDiskSpaceToolStripMenuItem_Click);
            // 
            // preferencesToolStripMenuItem
            // 
            this.preferencesToolStripMenuItem.Name = "preferencesToolStripMenuItem";
            this.preferencesToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.preferencesToolStripMenuItem.Text = "Preferences";
            this.preferencesToolStripMenuItem.Click += new System.EventHandler(this.preferencesToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(44, 20);
            this.toolStripMenuItem1.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // containerlistBox
            // 
            this.containerlistBox.FormattingEnabled = true;
            this.containerlistBox.HorizontalScrollbar = true;
            this.containerlistBox.Location = new System.Drawing.Point(12, 52);
            this.containerlistBox.Name = "containerlistBox";
            this.containerlistBox.Size = new System.Drawing.Size(120, 173);
            this.containerlistBox.TabIndex = 1;
            this.containerlistBox.SelectedIndexChanged += new System.EventHandler(this.containerlistBox_SelectedIndexChanged);
            // 
            // openButton
            // 
            this.openButton.Location = new System.Drawing.Point(13, 232);
            this.openButton.Name = "openButton";
            this.openButton.Size = new System.Drawing.Size(75, 23);
            this.openButton.TabIndex = 2;
            this.openButton.Text = "Open";
            this.openButton.UseVisualStyleBackColor = true;
            this.openButton.Click += new System.EventHandler(this.openButton_Click);
            // 
            // fileQueuelistBox
            // 
            this.fileQueuelistBox.FormattingEnabled = true;
            this.fileQueuelistBox.HorizontalScrollbar = true;
            this.fileQueuelistBox.Location = new System.Drawing.Point(165, 52);
            this.fileQueuelistBox.Name = "fileQueuelistBox";
            this.fileQueuelistBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.fileQueuelistBox.Size = new System.Drawing.Size(207, 173);
            this.fileQueuelistBox.TabIndex = 3;
            this.fileQueuelistBox.SelectedIndexChanged += new System.EventHandler(this.fileQueuelistBox_SelectedIndexChanged);
            // 
            // containerFilesFolderlistBox
            // 
            this.containerFilesFolderlistBox.FormattingEnabled = true;
            this.containerFilesFolderlistBox.HorizontalScrollbar = true;
            this.containerFilesFolderlistBox.Location = new System.Drawing.Point(460, 52);
            this.containerFilesFolderlistBox.Name = "containerFilesFolderlistBox";
            this.containerFilesFolderlistBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.containerFilesFolderlistBox.Size = new System.Drawing.Size(206, 173);
            this.containerFilesFolderlistBox.TabIndex = 4;
            // 
            // addfileButton
            // 
            this.addfileButton.Location = new System.Drawing.Point(165, 232);
            this.addfileButton.Name = "addfileButton";
            this.addfileButton.Size = new System.Drawing.Size(75, 23);
            this.addfileButton.TabIndex = 5;
            this.addfileButton.Text = "Add File...";
            this.addfileButton.UseVisualStyleBackColor = true;
            this.addfileButton.Click += new System.EventHandler(this.addfileButton_Click);
            // 
            // removeButton
            // 
            this.removeButton.Location = new System.Drawing.Point(297, 231);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(75, 23);
            this.removeButton.TabIndex = 6;
            this.removeButton.Text = "Remove";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // toContainerButton
            // 
            this.toContainerButton.Location = new System.Drawing.Point(378, 110);
            this.toContainerButton.Name = "toContainerButton";
            this.toContainerButton.Size = new System.Drawing.Size(75, 23);
            this.toContainerButton.TabIndex = 7;
            this.toContainerButton.Text = ">";
            this.toContainerButton.UseVisualStyleBackColor = true;
            this.toContainerButton.Click += new System.EventHandler(this.toContainerButton_Click);
            // 
            // fromContainerButton
            // 
            this.fromContainerButton.Location = new System.Drawing.Point(378, 139);
            this.fromContainerButton.Name = "fromContainerButton";
            this.fromContainerButton.Size = new System.Drawing.Size(75, 23);
            this.fromContainerButton.TabIndex = 8;
            this.fromContainerButton.Text = "<";
            this.fromContainerButton.UseVisualStyleBackColor = true;
            this.fromContainerButton.Click += new System.EventHandler(this.fromContainerButton_Click);
            // 
            // addFolderButton
            // 
            this.addFolderButton.Location = new System.Drawing.Point(165, 261);
            this.addFolderButton.Name = "addFolderButton";
            this.addFolderButton.Size = new System.Drawing.Size(75, 23);
            this.addFolderButton.TabIndex = 9;
            this.addFolderButton.Text = "Add Folder...";
            this.addFolderButton.UseVisualStyleBackColor = true;
            this.addFolderButton.Click += new System.EventHandler(this.addFolderButton_Click);
            // 
            // removeContainerButton
            // 
            this.removeContainerButton.Location = new System.Drawing.Point(13, 288);
            this.removeContainerButton.Name = "removeContainerButton";
            this.removeContainerButton.Size = new System.Drawing.Size(75, 23);
            this.removeContainerButton.TabIndex = 10;
            this.removeContainerButton.Text = "Remove";
            this.removeContainerButton.UseVisualStyleBackColor = true;
            this.removeContainerButton.Click += new System.EventHandler(this.removeContainerButton_Click);
            // 
            // closeButton
            // 
            this.closeButton.Location = new System.Drawing.Point(13, 259);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75, 23);
            this.closeButton.TabIndex = 11;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(678, 323);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.removeContainerButton);
            this.Controls.Add(this.addFolderButton);
            this.Controls.Add(this.fromContainerButton);
            this.Controls.Add(this.toContainerButton);
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.addfileButton);
            this.Controls.Add(this.containerFilesFolderlistBox);
            this.Controls.Add(this.fileQueuelistBox);
            this.Controls.Add(this.openButton);
            this.Controls.Add(this.containerlistBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximumSize = new System.Drawing.Size(694, 362);
            this.MinimumSize = new System.Drawing.Size(694, 362);
            this.Name = "Main";
            this.Text = "Yet Another Disk Encryption Utility";
            this.Load += new System.EventHandler(this.Main_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createEncryptedContainerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eraseFreeDiskSpaceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem preferencesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openEncryptedContainerToolStripMenuItem;
        private System.Windows.Forms.ListBox containerlistBox;
        private System.Windows.Forms.Button openButton;
        private System.Windows.Forms.ListBox fileQueuelistBox;
        private System.Windows.Forms.ListBox containerFilesFolderlistBox;
        private System.Windows.Forms.Button addfileButton;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.Button toContainerButton;
        private System.Windows.Forms.Button fromContainerButton;
        private System.Windows.Forms.Button addFolderButton;
        private System.Windows.Forms.Button removeContainerButton;
        private System.Windows.Forms.Button closeButton;
    }
}

