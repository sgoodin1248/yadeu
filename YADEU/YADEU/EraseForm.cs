﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace YADEU
{
    public partial class EraseForm : Form
    {
        public EraseForm()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void EraseForm_Load(object sender, EventArgs e)
        {
            string availableSpace = "";

            foreach( DriveInfo di in DriveInfo.GetDrives() )
            {
                if (di.Name == @"C:\") {
                    if (di.AvailableFreeSpace >= 1024 * 1024 * 1024)
                        availableSpace = string.Format("{0:0.0} GB", ((double)di.AvailableFreeSpace) / 1024.0 / 1024.0 / 1024.0);
                    else if (di.AvailableFreeSpace >= 1024 * 1024)
                        availableSpace = string.Format("{0:0.0} MB", ((double)di.AvailableFreeSpace) / 1024.0 / 1024.0);
                    else if (di.AvailableFreeSpace >= 1024)
                        availableSpace = string.Format("{0:0.0} KB", ((double)di.AvailableFreeSpace) / 1024.0);
                    else
                        availableSpace = di.AvailableFreeSpace.ToString() + " Bytes";
                }

                switch (Config.ErasureAlgorithm)
                {
                    case ErasureAlgorithm.Pseudorandom_1:
                        comboBox1.SelectedItem = "Pseudorandom (1 Pass)";
                        break;
                    case ErasureAlgorithm.Zeroes_1:
                        comboBox1.SelectedItem = "All Zeroes (1 Pass)";
                        break;
                    case ErasureAlgorithm.Ones_1:
                        comboBox1.SelectedItem = "All Ones (1 Pass)";
                        break;
                    case ErasureAlgorithm.DoD_5220_22_M_3:
                        comboBox1.SelectedItem = "US DoD 5220.22-M (3 Passes)";
                        break;
                    default:
                        comboBox1.SelectedItem = "Pseudorandom (1 Pass)";
                        break;
                }

                comboBox2.Items.Add(di.Name);
                comboBox2.SelectedItem = @"C:\";
                freeSpaceLabel.Text = availableSpace;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Erase.diskErase((string) comboBox2.SelectedItem);
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool matched = false;
            foreach (DriveInfo di in DriveInfo.GetDrives())
            {
                try
                {
                    if (di.Name == (string)comboBox2.SelectedItem)
                    {
                        matched = true;
                        if (di.AvailableFreeSpace >= 1024 * 1024 * 1024)
                            freeSpaceLabel.Text = string.Format("{0:0.0} GB", ((double)di.AvailableFreeSpace) / 1024.0 / 1024.0 / 1024.0);
                        else if (di.AvailableFreeSpace >= 1024 * 1024)
                            freeSpaceLabel.Text = string.Format("{0:0.0} MB", ((double)di.AvailableFreeSpace) / 1024.0 / 1024.0);
                        else if (di.AvailableFreeSpace >= 1024)
                            freeSpaceLabel.Text = string.Format("{0:0.0} KB", ((double)di.AvailableFreeSpace) / 1024.0);
                        else
                            freeSpaceLabel.Text = di.AvailableFreeSpace.ToString() + " Bytes";
                    }
                }
                catch { freeSpaceLabel.Text = "N/A"; }
            }
            if (!matched)
                freeSpaceLabel.Text = "";
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBox1.Text)
            {
                case "Pseudorandom (1 Pass)":
                    Config.ErasureAlgorithm = ErasureAlgorithm.Pseudorandom_1;
                    break;
                case "All Zeroes (1 Pass)":
                    Config.ErasureAlgorithm = ErasureAlgorithm.Zeroes_1;
                    break;
                case "All Ones (1 Pass)":
                    Config.ErasureAlgorithm = ErasureAlgorithm.Ones_1;
                    break;
                case "US DoD 5220.22-M (3 Passes)":
                    Config.ErasureAlgorithm = ErasureAlgorithm.DoD_5220_22_M_3;
                    break;
                default:
                    Config.ErasureAlgorithm = ErasureAlgorithm.Pseudorandom_1;
                    break;
            }
        }
    }
}
