﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace YADEU
{
    enum FileOrDirectory
    {
        File,
        Directory
    }
    class Container
    {
        public string filepath;
        public string drive;
        public string alias;
        public long nextKey;
        public List<Junction> junctions;
        public long size_bytes;
        public bool mounted;
                
        public void moveInto(Junction junction)
        {
            Global.copying = true;
            if (!mounted)
                return;            
            junction.actualPath = drive + "_" + nextKey.ToString();
            if (!File.Exists(junction.virtualPath) && !Directory.Exists(junction.virtualPath))
            {
                MessageBox.Show("Error: Nonexistent file or folder.", "Nonexistent File or Folder", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if (Directory.Exists(junction.virtualPath))
            {
                Directory.CreateDirectory(junction.actualPath);
                junction.type = FileOrDirectory.Directory;
                copyDirectory(junction.virtualPath, junction.actualPath);
                if (Global.copying)
                {
                    Erase.EraseDirectory(junction.virtualPath, true);
                    Junction.MakeJunction(junction);
                    using (StreamWriter sw = new StreamWriter(drive + @".yadeu\config", true))
                    {
                        sw.WriteLine(junction.virtualPath + "|" + junction.actualPath + "|" + "d");
                    }
                    junctions.Add(junction);
                }
            }
            else // File
            {
                junction.type = FileOrDirectory.File;
                copyFile(junction.virtualPath, junction.actualPath);
                if (Global.copying)
                {
                    Erase.EraseFile(junction.virtualPath, Config.ErasureAlgorithm);
                    Junction.MakeJunction(junction);
                    using (StreamWriter sw = new StreamWriter(drive + @".yadeu\config", true))
                    {
                        sw.WriteLine(junction.virtualPath + "|" + junction.actualPath + "|" + "f");
                    }
                    junctions.Add(junction);
                }
            }
            IncrementKey();
            Global.copying = false;
        }

        public void moveBack(Junction junction)
        {
            Global.copying = true;
            if (!mounted)
                return;
            bool found = false;
            using (StreamReader sr = new StreamReader(drive + @".yadeu\config"))
            {
                string line;
                Regex configregex = new Regex(@"(.*?)\|(.*?)\|(.*)");
                while (!sr.EndOfStream)
                {
                    line = sr.ReadLine();
                    if ( configregex.IsMatch(line))
                    {
                        Match match = configregex.Match(line);
                        if ( junction.virtualPath == match.Groups[1].ToString() )
                        {
                            junction.type = (match.Groups[3].ToString().Trim() == "d" ? FileOrDirectory.Directory : FileOrDirectory.File);
                            junction.actualPath = match.Groups[2].ToString();
                            found = true;
                        }
                    }
                }
            }
            
            if ( !found )
            {
                return;
            }

            if ( junction.type == FileOrDirectory.Directory )
            {
                Junction.DeleteJunction(junction);
                copyDirectory(junction.actualPath, junction.virtualPath);
                if (Global.copying)
                {
                    Erase.EraseDirectory(junction.actualPath, false);
                    DeleteEntry(junction);
                }
            }
            else // File
            {
                Junction.DeleteJunction(junction);
                copyFile(junction.actualPath, junction.virtualPath);
                DeleteEntry(junction);
                File.Delete(junction.actualPath);
            }
            Junction removejunction = junctions.FirstOrDefault(j => j.virtualPath == junction.virtualPath);
            if (removejunction != null)
                junctions.Remove(removejunction);
            Global.copying = false;
        }

        public static Container Open(string _alias, string _filepath, string _drive)
        {
            Container container = new Container(_alias, _filepath, _drive);
            container.Open();
            return container;
        }

        public void Open()
        {
            if (mounted == false)
            {
                Process tc = new Process();
                tc.StartInfo.FileName = "TrueCrypt.exe";
                tc.StartInfo.Arguments = @"/q /v """ + filepath + "\" /l" + drive[0].ToString().ToLower() + " /p \"" + Global.password.Replace("\"", "\"\"") + "\"";
                tc.Start();
                tc.WaitForExit();
            }

            foreach (DriveInfo driveinfo in DriveInfo.GetDrives())
            {
                if (driveinfo.Name == drive && driveinfo.IsReady)
                {
                    mounted = true;
                }
            }

            if (!mounted)
                return;

            if (!Directory.Exists(drive + ".yadeu"))
            {
                Directory.CreateDirectory(drive + ".yadeu");
                DirectoryInfo di = new DirectoryInfo(drive + ".yadeu");
                di.Attributes = FileAttributes.Hidden;
            }

            if (!File.Exists(drive + @".yadeu\config"))
            {
                using (StreamWriter sw = new StreamWriter(drive + @".yadeu\config"))
                {
                    sw.WriteLine("Next Key: 0");
                }
            }
            else
            {
                junctions.Clear();
                using (StreamReader sr = new StreamReader(File.Open(drive + @".yadeu\config", FileMode.Open, FileAccess.Read)))
                {
                    // <virtual path>|<real path>|<d or f>
                    Regex configregex = new Regex(@"(.*?)\|(.*?)\|(.*)");

                    Regex nextkeyregex = new Regex("Next Key: *(.*)");

                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();
                        if (configregex.IsMatch(line))
                        {
                            Match match = configregex.Match(line);
                            Junction jnctn = new Junction(match.Groups[1].ToString(), match.Groups[2].ToString(), match.Groups[3].ToString().Trim() == "d" ? FileOrDirectory.Directory : FileOrDirectory.File);
                            junctions.Add(jnctn);
                        }
                        else if (nextkeyregex.IsMatch(line))
                        {
                            this.nextKey = Convert.ToInt64(nextkeyregex.Match(line).Groups[1].ToString());
                        }
                    }
                }
            }
            foreach(DriveInfo di in DriveInfo.GetDrives())
            {
                if (di.Name == drive)
                    size_bytes = di.AvailableFreeSpace;
            }
        }

        public void Close()
        {
            junctions.Clear();
            Process tc = new Process();
            tc.StartInfo.FileName = "TrueCrypt.exe";
            tc.StartInfo.Arguments = @"/q /d" + drive[0].ToString().ToLower();
            tc.Start();
            tc.WaitForExit();
            mounted = false;
        }

        public Container(string _alias, string _filepath, string _drive) {
            filepath = _filepath;
            drive = _drive;
            alias = _alias;
            nextKey = 1;
            size_bytes = long.MaxValue;
            mounted = false;
            junctions = new List<Junction>();

            foreach (DriveInfo driveinfo in DriveInfo.GetDrives())
            {
                if (driveinfo.Name == drive && driveinfo.IsReady)
                {
                    mounted = true;
                }
            }

            if (!mounted || !File.Exists(drive + @".yadeu\config"))
                return;

            using (StreamReader sr = new StreamReader(File.Open(drive + @".yadeu\config", FileMode.Open, FileAccess.Read)))
            {
                // <virtual path>|<real path>|<d or f>
                Regex configregex = new Regex(@"(.*?)\|(.*?)\|(.*)");

                Regex nextkeyregex = new Regex("Next Key: *(.*)");

                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    if (configregex.IsMatch(line))
                    {
                        Match match = configregex.Match(line);
                        Junction jnctn = new Junction(match.Groups[1].ToString(), match.Groups[2].ToString(), match.Groups[3].ToString().Trim() == "d" ? FileOrDirectory.Directory : FileOrDirectory.File);
                        junctions.Add(jnctn);
                    }
                    else if (nextkeyregex.IsMatch(line))
                    {
                        this.nextKey = Convert.ToInt64(nextkeyregex.Match(line).Groups[1].ToString());
                    }
                }
            }            
        } 

        private void copyDirectory(string source, string destination)
        {
            Global.copying = true;
            Global.copyLock = true;

            DirectoryInfo di = new DirectoryInfo(source);

            Directory.CreateDirectory(destination);

            Application.DoEvents();

            foreach (DirectoryInfo d in di.GetDirectories())
            {
                if (Global.copying)
                {                    
                    copySubDirectory(source + @"\" + d.Name, destination + @"\" + d.Name);
                }
                else
                {
                    Global.copyLock = false;
                    break;
                }
            }

            foreach (FileInfo fi in di.GetFiles())
            {
                Application.DoEvents();
                if (Global.copying)
                {
                    File.Copy(source + @"\" + fi.Name, destination + @"\" + fi.Name);
                }
                else
                {
                    Global.copyLock = false;
                    break;
                }
            }            
            Global.copyLock = false;
        }

        private void copySubDirectory(string source, string destination)
        {
            DirectoryInfo di = new DirectoryInfo(source);

            Directory.CreateDirectory(destination);

            Application.DoEvents();

            foreach (DirectoryInfo d in di.GetDirectories())
            {
                if (Global.copying)
                {                    
                    copySubDirectory(source + @"\" + d.Name, destination + @"\" + d.Name);
                }
                else
                {
                    Global.copyLock = false;
                    break;
                }
            }

            foreach(FileInfo fi in di.GetFiles())
            {
                Application.DoEvents();
                if ( Global.copying )
                {
                    File.Copy(source + @"\" + fi.Name, destination + @"\" + fi.Name);
                }
                else
                {
                    Global.copyLock = false;
                    break;
                }
            }

            if (!Global.copying)
                Global.copyLock = false;
        }

        private void copyFile(String source, string destination)
        {
            Global.copying = true;
            Global.copyLock = true;
            Application.DoEvents();
            File.Copy(source, destination, true);
            Global.copyLock = false;
        }

        private void IncrementKey()
        {
            string line;
            StringBuilder sb = new StringBuilder("");

            using (StreamReader sr = new StreamReader(drive + @".yadeu\config"))
            {
                Regex nextkeyregex = new Regex("Next Key: *(.*)");

                while (!sr.EndOfStream)
                {
                    line = sr.ReadLine();

                    if (nextkeyregex.IsMatch(line))
                    {
                        nextkeyregex.Match(line).Groups[1].ToString();
                        sb.AppendLine("Next Key: " + (++nextKey).ToString());
                    }
                    else
                        sb.AppendLine(line);
                }
            }

            using (StreamWriter sw = new StreamWriter(drive + @".yadeu\config", false))
            {
                sw.Write(sb.ToString());
            }
        }


        private void DeleteEntry(Junction junction)
        {
            StringBuilder fileText = new StringBuilder("");
            string line;
            Regex configregex = new Regex(@"(.*?)\|(.*?)\|(.*)");

            using (StreamReader sr = new StreamReader(drive + @".yadeu\config"))
            {
                while ( !sr.EndOfStream )
                {
                    line = sr.ReadLine();
                    if ( configregex.IsMatch(line) )
                    {
                        Match match = configregex.Match(line);
                        if (!(junction.virtualPath == match.Groups[1].ToString()) || !((match.Groups[3].ToString() == "d" ? FileOrDirectory.Directory : FileOrDirectory.File) == junction.type))
                            fileText.AppendLine(line);
                    }
                    else
                    {
                        fileText.AppendLine(line);
                    }
                }
            }

            using (StreamWriter sw = new StreamWriter(drive + @".yadeu\config", false))
            {
                sw.Write(fileText.ToString());
            }
        }
    }

    class Junction
    {
        public string virtualPath;
        public string actualPath;
        public FileOrDirectory type;

        [DllImport("kernel32.dll")]
        private static extern bool CreateSymbolicLink(string lpSymlinkFileName, string lpTargetFileName, int dwFlags);
        
        public Junction(string _virtualPath, string _actualPath, FileOrDirectory _type )
        {
            virtualPath = _virtualPath;
            actualPath = _actualPath;
            type = _type;
        }

        public static void MakeJunction(Junction junction)
        {
            CreateSymbolicLink(junction.virtualPath, junction.actualPath, junction.type == FileOrDirectory.Directory ? 1 : 0);
        }

        public static void DeleteJunction(Junction junction)
        {
            if ( junction.type == FileOrDirectory.Directory )
            {
                if ( Directory.Exists(junction.virtualPath))
                    Directory.Delete(junction.virtualPath);
            }
            else
            {
                if ( File.Exists(junction.virtualPath))
                    File.Delete(junction.virtualPath);
            }
        }

        private Junction() { }
    }
}
