﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YADEU
{
    public partial class EraseHibernationFileProgress : Form
    {
        public EraseHibernationFileProgress()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Erase.stopFileErase();
            this.Close();
        }
    }
}
