﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YADEU
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {

            (new About()).ShowDialog(this);
        }

        private void preferencesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Preferences preferences = new Preferences();
            preferences.ShowDialog();
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            Global.containerAlias = (string) containerlistBox.SelectedItem;
            YADEU.Container container = Config.Containers.FirstOrDefault(c => c.alias == Global.containerAlias);

            Global.containerPath = container.filepath;
            OpenContainerForm ocf = new OpenContainerForm();
            ocf.ShowDialog();
            RefreshContainerList();

            if ( container.mounted )
            {
                addfileButton.Enabled = true;
                addFolderButton.Enabled = true;
                toContainerButton.Enabled = true;
                fromContainerButton.Enabled = true;
            }

        }

        private void openEncryptedContainerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Global.containerAlias = "";
            OpenFileDialog fd = new OpenFileDialog();
            fd.DefaultExt = "tc";
            fd.CheckFileExists = true;
            fd.Filter = "TrueCrypt Container (*.tc)|*.tc|All files (*.*)|*.*";
            fd.ShowDialog();
            fd.Multiselect = false;

            if (fd.FileName == "")
                return;

            Global.containerPath = fd.FileName;
            OpenContainerForm ocf = new OpenContainerForm();
            ocf.ShowDialog();
            RefreshContainerList();
        }

        private void createEncryptedContainerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process pcfg = new Process();
            pcfg.StartInfo.FileName = "TrueCrypt Format.exe";
            pcfg.Start();
            pcfg.WaitForExit();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            RefreshContainerList();
            openButton.Enabled = false;
            closeButton.Enabled = false;
            removeContainerButton.Enabled = false;
            addfileButton.Enabled = false;
            removeButton.Enabled = false;
            addFolderButton.Enabled = false;
            toContainerButton.Enabled = false;
            fromContainerButton.Enabled = false;
        }

        private void eraseFreeDiskSpaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EraseForm ef = new EraseForm();
            ef.ShowDialog();
        }

        private void RefreshContainerList()
        {
            containerlistBox.Items.Clear();

            List<Container> existing = new List<YADEU.Container>();
            
            StringBuilder fileText = new StringBuilder("");
            using (StreamReader sr = new StreamReader("config"))
            {                
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    Regex containerregex = new Regex(@"(.*?)\|(.*?)\|(.*)");
                    if ( containerregex.IsMatch(line))
                    {
                        Match match = containerregex.Match(line);
                        string alias = match.Groups[1].ToString();
                        string filepath = match.Groups[2].ToString();
                        string drive = match.Groups[3].ToString();
                        Container container = Config.Containers.FirstOrDefault(c => c.alias == alias);
                        existing.Add(container);
                        if ( container != null )
                        {
                            fileText.AppendLine(container.alias + "|" + container.filepath + "|" + container.drive[0].ToString());
                        }
                    }
                    else
                    {
                        fileText.AppendLine(line);
                    }
                }                
            }

            foreach (Container c in Config.Containers)
            {
                containerlistBox.Items.Add(c.alias);
                if ( !existing.Exists(v => v.alias == c.alias) )
                {
                    fileText.AppendLine(c.alias + "|" + c.filepath + "|" + c.drive[0].ToString());
                }                
            }

            using (StreamWriter sw = new StreamWriter("config"))
            {
                sw.Write(fileText.ToString());
            }
        }

        private void containerlistBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Container container = Config.Containers.FirstOrDefault(c => c.alias == (string) containerlistBox.SelectedItem);
            Global.containerAlias = (string) containerlistBox.SelectedItem;

            fileQueuelistBox.Items.Clear();
            containerFilesFolderlistBox.Items.Clear();

            if (container != null && container.mounted)
            {
                foreach (Junction junction in container.junctions)
                {
                    containerFilesFolderlistBox.Items.Add(junction.virtualPath);
                }
            }

            if (container != null && container.mounted)
            {
                openButton.Enabled = false;
                closeButton.Enabled = true;
                removeButton.Enabled = false;
                fromContainerButton.Enabled = true;
                toContainerButton.Enabled = true;
                addfileButton.Enabled = true;
                addFolderButton.Enabled = true;
                removeContainerButton.Enabled = true;
            }
            else if (container != null)
            {
                openButton.Enabled = true;
                closeButton.Enabled = false;
                removeButton.Enabled = true;
                fromContainerButton.Enabled = false;
                toContainerButton.Enabled = false;
                addfileButton.Enabled = false;
                addFolderButton.Enabled = false;
                removeContainerButton.Enabled = false;
            }
            else
            {
                openButton.Enabled = false;
                closeButton.Enabled = false;
                openButton.Enabled = false;
                fromContainerButton.Enabled = false;
                toContainerButton.Enabled = false;
                addfileButton.Enabled = false;
                addFolderButton.Enabled = false;
                removeContainerButton.Enabled = false;
                removeButton.Enabled = false;
            }
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            Container container = Config.Containers.FirstOrDefault(c => c.alias == (string)containerlistBox.SelectedItem);

            if (container != null && container.mounted == true)
                container.Close();
            else if (container != null)
                MessageBox.Show("Volume not mounted.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            openButton.Enabled = true;
            closeButton.Enabled = false;
            containerFilesFolderlistBox.Items.Clear();
            fileQueuelistBox.Items.Clear();
        }

        private void removeContainerButton_Click(object sender, EventArgs e)
        {
            Container container = Config.Containers.FirstOrDefault(c => c.alias == (string)containerlistBox.SelectedItem);

            if (container != null && !container.mounted)
                Config.Containers.Remove(container);
            else if (container != null)
                MessageBox.Show("Volume is not mounted.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            RefreshContainerList();
        }

        private void toContainerButton_Click(object sender, EventArgs e)
        {
            YADEU.Container container = Config.Containers.FirstOrDefault(c => c.alias == Global.containerAlias);
            
            foreach (var selection in fileQueuelistBox.SelectedItems)
            {
                if (selection.ToString() == "")
                    continue;
                Junction junction = new Junction(selection.ToString(), "", FileOrDirectory.Directory);                
                CopyProgress progress = new CopyProgress();
                if (container == null && !container.junctions.Exists(j => j.virtualPath == selection.ToString()) )
                    continue;
                progress.task = (() => { container.moveInto(junction); });
                progress.ShowDialog();
            }

            List<object> selected = new List<object>();

            foreach (object o in fileQueuelistBox.SelectedItems)
            {
                selected.Add(o);
            }

            foreach(object o in selected)
            {
                fileQueuelistBox.Items.Remove(o);
            }

            containerFilesFolderlistBox.Items.Clear();

            if (container != null && container.mounted)
            {
                foreach (Junction junction in container.junctions)
                {
                    containerFilesFolderlistBox.Items.Add(junction.virtualPath);
                }
            }
        }

        private void fromContainerButton_Click(object sender, EventArgs e)
        {
            YADEU.Container container = Config.Containers.FirstOrDefault(c => c.alias == containerlistBox.SelectedItem.ToString());

            if (container == null)
                return;

            foreach (var selection in containerFilesFolderlistBox.SelectedItems)
            {
                if (selection.ToString() == "")
                    continue;
                Junction junction = new Junction(selection.ToString(), "", FileOrDirectory.Directory);
                CopyProgress progress = new CopyProgress();
                progress.task = (() => { container.moveBack(junction); });
                progress.ShowDialog();
            }

            containerFilesFolderlistBox.Items.Clear();

            if (container != null && container.mounted)
            {
                foreach (Junction junction in container.junctions)
                {
                    containerFilesFolderlistBox.Items.Add(junction.virtualPath);
                }
            }
        }

        private void addfileButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.ShowDialog();
            dialog.Multiselect = true;
            string[] paths = dialog.FileNames;

            foreach(string path in paths)
            {
                if ( path != "" )
                    fileQueuelistBox.Items.Add(path);
            }
        }

        private void addFolderButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.ShowDialog();
            if ( dialog.SelectedPath != "" )
                fileQueuelistBox.Items.Add(dialog.SelectedPath);
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            List<object> remove = new List<object>();
            foreach (var selection in fileQueuelistBox.SelectedItems)
            {
                remove.Add(selection);
            }

            foreach(object o in remove)
            {
                fileQueuelistBox.Items.Remove(o);
            }
        }

        private void fileQueuelistBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (fileQueuelistBox.SelectedItems.Count > 0)
                removeButton.Enabled = true;
            else
                removeButton.Enabled = false;
        }
    }
}
