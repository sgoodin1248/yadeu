﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YADEU
{
    public partial class CopyProgress : Form
    {
        public CopyProgress()
        {
            InitializeComponent();
        }

        public Action task;

        private void button1_Click(object sender, EventArgs e)
        {
            var response = MessageBox.Show("Are you sure you want to stop the copy process?", "Abort Process?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
            if (response == DialogResult.No)
                return;
            Global.copying = false;
            while ( Global.copyLock )
            {
                System.Threading.Thread.Sleep(100);
                Application.DoEvents();
            }
        }

        private void CopyProgress_Load(object sender, EventArgs e)
        {
            task();
            this.Close();
        }
    }
}
