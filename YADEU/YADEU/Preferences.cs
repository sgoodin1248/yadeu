﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Diagnostics;

namespace YADEU
{
    public partial class Preferences : Form
    {
        public Preferences()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            bool eraseHibernationFile = false;
            ErasureAlgorithm oldEraseAlgorithm = Config.ErasureAlgorithm;
            
            if (checkBox1.CheckState == CheckState.Checked)
            {
                if (!File.Exists(Environment.GetEnvironmentVariable("APPDATA") + @"\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\YADEU.exe"))
                {
                    string sourcepath = Assembly.GetExecutingAssembly().Location;
                    string destinationpath = Environment.GetEnvironmentVariable("APPDATA") + @"\Microsoft\Windows\Start Menu\Programs\Startup\YADEU.exe";
                    byte[] file = File.ReadAllBytes(sourcepath);
                    File.WriteAllBytes(destinationpath, file);
                }
            }
            else
            {
                if (File.Exists(Environment.GetEnvironmentVariable("APPDATA") + @"\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\YADEU.exe"))
                    File.Delete(Environment.GetEnvironmentVariable("APPDATA") + @"\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\YADEU.exe");
            }            

            bool promptRestart = false;

            if (checkBox2.Checked == false && !File.Exists(@"C:\hiberfil.sys"))
            {
                Process pcfg = new Process();
                pcfg.StartInfo.FileName = "powercfg.exe";
                pcfg.StartInfo.Arguments = @"/hibernate on";
                pcfg.Start();
                pcfg.WaitForExit();
                promptRestart = true;
            }

            else if (checkBox2.Checked == true && File.Exists(@"C:\hiberfil.sys"))
            {
                Process pcfg = new Process();
                pcfg.StartInfo.FileName = "powercfg.exe";
                pcfg.StartInfo.Arguments = @"/hibernate off";
                pcfg.Start();
                pcfg.WaitForExit();
                eraseHibernationFile = true;
                promptRestart = true;
            }            

            var key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Control\Session Manager\Memory Management", true);

            int regval = (int) key.GetValue("ClearPageFileAtShutdown", 2);

            if (regval == 2)
                key.SetValue("ClearPageFileAtShutdown", 1);

            regval = (int) key.GetValue("ClearPageFileAtShutdown", 2);

            if (checkBox3.Checked == true && regval == 0)
            {
                key.SetValue("ClearPageFileAtShutdown", 1);
                promptRestart = true;
            }
            else if (!checkBox3.Checked && regval == 1)
            {
                key.SetValue("ClearPageFileAtShutdown", 0);
                promptRestart = true;
            }

            switch (comboBox2.Text)
            {
                case "Pseudorandom (1 Pass)":
                    Config.ErasureAlgorithm = ErasureAlgorithm.Pseudorandom_1;
                    break;
                case "All Zeroes (1 Pass)":
                    Config.ErasureAlgorithm = ErasureAlgorithm.Zeroes_1;
                    break;
                case "All Ones (1 Pass)":
                    Config.ErasureAlgorithm = ErasureAlgorithm.Ones_1;
                    break;
                case "US DoD 5220.22-M (3 Passes)":
                    Config.ErasureAlgorithm = ErasureAlgorithm.DoD_5220_22_M_3;
                    break;
                default:
                    Config.ErasureAlgorithm = ErasureAlgorithm.Pseudorandom_1;
                    break;
            }

            using (StreamWriter sw = new StreamWriter(File.Create("config")))
            {
                sw.WriteLine("WipeDefault = " + Config.ErasureAlgorithm.ToString());
            }

            if ( eraseHibernationFile )
            {
                if (File.Exists(@"C:\hiberfil.sys"))
                {
                    var response = MessageBox.Show("It is strongly recommended to securely delete the hibernation file. Do you want to delete the hibernation file (process could take from a few minutes to a few hours depending on disk access speed, etc.)?", "Erase Hibernation File?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (response == DialogResult.Yes)
                    {
                        EraseHibernationFileProgress ehfp = new EraseHibernationFileProgress();
                        ehfp.ShowDialog();
                        Erase.EraseFile(@"C:\hiberfil.sys", oldEraseAlgorithm);
                        ehfp.Close();
                    }
                }
                else
                {
                    var response = MessageBox.Show("It is strongly recommended to wipe free disk space to remove sensitive information in the hibernation file. Do you want to sanitize free disk space (process could take from a few minutes to a few hours depending on disk access speed, available space to wipe, etc.)?", "Erase Disk Space?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    Erase.diskErase(@"C:\");
                }
            }

            if (promptRestart) {
                DialogResult result = MessageBox.Show("Changes in settings require a restart to apply. Would you like to restart now?", "Restart Required", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if ( result == DialogResult.Yes )
                {
                    Process pcfg = new Process();
                    pcfg.StartInfo.FileName = "shutdown.exe";
                    pcfg.StartInfo.Arguments = @"/f /r /t 0";
                    pcfg.Start();
                    pcfg.WaitForExit();
                }
            }

            this.Close();
        }

        private void Preferences_Load(object sender, EventArgs e)
        {
            
            switch (Config.ErasureAlgorithm)
            {
                case ErasureAlgorithm.Pseudorandom_1:
                    comboBox2.SelectedItem = "Pseudorandom (1 Pass)";
                    break;
                case ErasureAlgorithm.Zeroes_1:
                    comboBox2.SelectedItem = "All Zeroes (1 Pass)";
                    break;
                case ErasureAlgorithm.Ones_1:
                    comboBox2.SelectedItem = "All Ones (1 Pass)";
                    break;
                case ErasureAlgorithm.DoD_5220_22_M_3:
                    comboBox2.SelectedItem = "US DoD 5220.22-M (3 Passes)";
                    break;
                default:
                    comboBox2.SelectedItem = "Pseudorandom (1 Pass)";
                    break;
            }

            if (File.Exists(Environment.GetEnvironmentVariable("APPDATA") + @"\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\YADEU.exe"))
                checkBox1.Checked = true;
            else
                checkBox1.Checked = false;

            if (!File.Exists(@"C:\hiberfil.sys"))
                checkBox2.Checked = true;
            else
                checkBox2.Checked = false;

            string regval = Microsoft.Win32.Registry.GetValue(@"HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Memory Management", "ClearPageFileAtShutdown", "").ToString();

            if (regval == "" || regval == "0")
                checkBox3.Checked = false;
            else
                checkBox3.Checked = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
