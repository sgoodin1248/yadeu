﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YADEU
{
    public partial class OpenContainerForm : Form
    {
        public OpenContainerForm()
        {
            InitializeComponent();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
                textBox1.UseSystemPasswordChar = false;
            else
                textBox1.UseSystemPasswordChar = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Config.Containers.Exists((c => (c.alias == Global.containerAlias || c.filepath == Global.containerPath) && c.mounted == true))) {
                MessageBox.Show("Container is already open.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Global.password = textBox1.Text;

            if ( textBox2.Text != "" )
            {
                Global.containerAlias = textBox2.Text;
            }
            else
            {
                MessageBox.Show("Volume must be given a name.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Container container = YADEU.Container.Open(Global.containerAlias, Global.containerPath, (string) comboBox1.SelectedItem);
            
            if (container.mounted == true)
            {
                MessageBox.Show("Encrypted volume mounted at " + container.drive, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (!Config.Containers.Exists(c => c.alias == container.alias))
                    Config.Containers.Add(container);
                else
                {
                    Container original = Config.Containers.First(c => c.alias == container.alias);
                    original.filepath = container.filepath;
                    original.junctions = container.junctions;
                    original.nextKey = container.nextKey;
                    original.mounted = true;
                    original.size_bytes = container.size_bytes;
                }
                this.Close();
            }
            else
                MessageBox.Show("Failure to open container. Make sure that the password entered is correct or that the container is not already open.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);     
        }

        private void OpenContainerForm_Load(object sender, EventArgs e)
        {
            Container container = Config.Containers.FirstOrDefault(f => f.filepath == Global.containerPath);
            if (  container != null )
            {
                textBox2.Enabled = false;
                textBox2.Text = Global.containerAlias;
                comboBox1.Enabled = false;
                comboBox1.Items.Add(container.drive);
                comboBox1.SelectedIndex = 0;
                return;
            }

            List<string> drives = new List<string>(new string[21] { @"F:\", @"G:\", @"H:\", @"I:\", @"J:\", @"K:\", @"L:\", @"M:\", @"N:\", @"O:\", @"P:\", @"Q:\", @"R:\", @"S:\", @"T:\", @"U:\", @"V:\", @"W:\", @"X:\", @"Y:\", @"Z:\" });

            foreach(DriveInfo di in DriveInfo.GetDrives())
            {
                if (drives.Contains(di.Name))
                    drives.Remove(di.Name);
            }

            foreach(string d in drives)
            {
                comboBox1.Items.Add(d);
            }

            comboBox1.SelectedIndex = 0;
        }
    }
}
